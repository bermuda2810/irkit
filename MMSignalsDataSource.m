#import "MMSignalsDataSource.h"
#import "IRDatabase.h"
@interface MMSignalsDataSource ()

@property (nonatomic) NSMutableArray *actions;

@end

@implementation MMSignalsDataSource

- (instancetype) init {
    self = [super init];
    if (! self) { return nil; }

//    _actions = [[IRSignals alloc] init];
    _actions = [[NSMutableArray alloc]init];

    return self;
}

#pragma mark - IRSignals delegate

- (void)addSignalsObject: (ItemAction*) action {
    [_actions addObject:action];
}

- (ItemAction*)objectAtIndex: (NSUInteger) index {
    return [_actions objectAtIndex:index];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_actions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MMSignalCell"];
    ItemAction *action = [_actions objectAtIndex:indexPath.row];
    cell.textLabel.text = action.signal.name;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        ItemAction *action = [_actions objectAtIndex:indexPath.row];
        [IRDatabase deleteSignal:action.action_id];
        [_actions removeObjectAtIndex:indexPath.row];
//        NSArray *indexs =  [[NSArray alloc] initWithObjects:indexPath, nil];
//        [tableView reloadRowsAtIndexPaths:indexs withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView reloadData];
    }
}

@end
