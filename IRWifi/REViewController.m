//
//  REViewController.m
//  IRWifi
//
//  Created by VietBQ on 5/13/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import "REViewController.h"
#import "Log.h"
#import "MMSignalsDataSource.h"
#import "IRDatabase.h"

@interface REViewController (){
    IRSignal *_signal;
}

@property (nonatomic) MMSignalsDataSource *datasource;

@end

@implementation REViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _datasource = [[MMSignalsDataSource alloc] init];
   
    NSArray *listSignal = [IRDatabase getListSignal];
    for (int i=0; i<[listSignal count]; i++) {
        [_datasource addSignalsObject:[listSignal objectAtIndex:i]];
    }
    self.tbl_signal.delegate = self;
    self.tbl_signal.dataSource = _datasource;
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // find IRKit if none is known
    if ([IRKit sharedInstance].countOfReadyPeripherals == 0) {
        IRNewPeripheralViewController *vc = [[IRNewPeripheralViewController alloc] init];
        vc.delegate = self;
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             NSLog(@"presented");
                         }];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onTestClick:(id)sender {
    if ( [IRKit sharedInstance].countOfReadyPeripherals == 0 ) {
        IRNewPeripheralViewController *vc = [[IRNewPeripheralViewController alloc] init];
        vc.delegate = self;
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             LOG(@"presented");
                         }];
    }
    else {
        IRNewSignalViewController *vc = [[IRNewSignalViewController alloc] init];
        vc.delegate = self;
        [self presentViewController:vc
                           animated:YES
                         completion:^{
                             LOG(@"presented");
                         }];
    }}

#pragma mark - IRNewPeripheralViewControllerDelegate

- (void)newPeripheralViewController:(IRNewPeripheralViewController *)viewController
            didFinishWithPeripheral:(IRPeripheral *)peripheral {
    LOG( @"peripheral: %@", peripheral );
    
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 LOG(@"dismissed");
                             }];
}

#pragma mark - IRNewSignalViewControllerDelegate

- (void)newSignalViewController:(IRNewSignalViewController *)viewController
            didFinishWithSignal:(IRSignal *)signal {
    LOG( @"signal: %@", signal);
    if (signal) {
        NSNumber *action_id = [IRDatabase insertSignal:signal];
        ItemAction *new_action = [[ItemAction alloc]init];
        
        new_action.action_id = action_id;
        new_action.signal = signal;
        
        [_datasource addSignalsObject:new_action];
        [self.tbl_signal reloadData];
    }
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 LOG(@"dismissed");
                             }];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LOG(@"indexPath: %@", indexPath);
    [tableView deselectRowAtIndexPath:indexPath
                             animated:YES];
    
    IRSignal *signal = [_datasource objectAtIndex:indexPath.row].signal;
    [signal sendWithCompletion:^(NSError *error) {
        LOG(@"sent error: %@", error);
    }];
}


@end
