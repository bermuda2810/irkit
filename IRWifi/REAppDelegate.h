//
//  REAppDelegate.h
//  IRWifi
//
//  Created by VietBQ on 5/13/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface REAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
