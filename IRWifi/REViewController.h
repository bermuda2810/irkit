//
//  REViewController.h
//  IRWifi
//
//  Created by VietBQ on 5/13/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <IRKit/IRKit.h>

@interface REViewController : UIViewController<IRNewPeripheralViewControllerDelegate,IRNewSignalViewControllerDelegate,UITableViewDelegate>
@property IBOutlet UIButton *btn_test;
@property IBOutlet UITableView *tbl_signal;


@end
