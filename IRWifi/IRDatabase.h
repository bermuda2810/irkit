//
//  IRDatabase.h
//  IRWifi
//
//  Created by VietBQ on 5/14/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IRKit/IRKit.h>
@class FMDatabase;

@interface IRDatabase : NSObject

+(void)createDatabase;
+(NSNumber *)insertSignal:(IRSignal *)signal;
+(int)editSignal:(int)idSignal signal:(IRSignal *)signal;
+(BOOL)deleteSignal:(NSNumber *)idSignal;
+(NSArray*)getListSignal;

@end
