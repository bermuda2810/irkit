//
//  ItemAction.h
//  IRWifi
//
//  Created by VietBQ on 5/15/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IRKit/IRKit.h>

@interface ItemAction : NSObject
@property(nonatomic) IRSignal *signal;
@property(nonatomic) NSNumber *action_id;
@end
