//
//  main.m
//  IRWifi
//
//  Created by VietBQ on 5/13/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "REAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([REAppDelegate class]));
    }
}
