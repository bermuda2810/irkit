//
//  IRDatabase.m
//  IRWifi
//
//  Created by VietBQ on 5/14/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import "IRDatabase.h"
#import "sqlite3.h"
#import "FMDatabase.h"
#import "ItemAction.h"

@implementation IRDatabase{
   
}

+(void)createDatabase{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"IRWifi.sqlite"];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"CREATE TABLE signal (id INTEGER  PRIMARY KEY AUTOINCREMENT,name TEXT ,deviceid TEXT,format TEXT,freq INTEGER,hostname TEXT,type TEXT,data TEXT,custom TEXT)"];
    [database close];
}

+(NSNumber *)insertSignal:(IRSignal *)signal{
    NSDictionary *infoSignal = [signal asDictionary];
    NSMutableDictionary *dataForInsert = [[NSMutableDictionary alloc]init];
    NSArray *data = [infoSignal objectForKey:@"data"];
    NSMutableString *rawData = [[NSMutableString alloc]init];
    for (int i = 0; i<[data count]; i ++) {
        [rawData appendFormat:@"%@",[data objectAtIndex:i]];
        if (i < [data count] -1) {
            [rawData appendString:@","];
        }
    }
    NSString *format = [infoSignal objectForKey:@"format"];
    NSString *custom = [infoSignal objectForKey:@"custom"];
    NSString *deviceid = [infoSignal objectForKey:@"deviceid"];
    NSString *hostname = [infoSignal objectForKey:@"hostname"];
    NSNumber *freq = [infoSignal objectForKey:@"freq"];
    NSString *type = [infoSignal objectForKey:@"type"];
    NSString *name = [infoSignal objectForKey:@"name"];
    
    [dataForInsert setObject:format  forKey:@"format"];
    [dataForInsert setObject:custom forKey:@"custom"];
    [dataForInsert setObject:deviceid forKey:@"deviceid"];
    [dataForInsert setObject:rawData forKey:@"data"];
    [dataForInsert setObject:hostname forKey:@"hostname"];
    [dataForInsert setObject:freq forKey:@"freq"];
    [dataForInsert setObject:type forKey:@"type"];
    [dataForInsert setObject:name forKey:@"name"];

    
    FMDatabase *database = [IRDatabase openDatabase];
    [database open];
    BOOL rs = [database executeUpdate:@"INSERT INTO signal (format,custom,deviceid,data,hostname,freq,type,name) VALUES (?,?,?,?,?,?,?,?)" ,format,custom,deviceid,rawData,hostname,freq,type,name];
    sqlite_int64 lastId = [database lastInsertRowId];
    NSLog(@"id = %lld",lastId);
    NSNumber *idAction = [NSNumber numberWithLongLong:lastId];
    [database close];
    NSLog(@"Result = %d",rs);
    return idAction;
}

+(int)editSignal:(int)idSignal signal:(IRSignal *)signal{
    return 1;
}

+(BOOL)deleteSignal:(NSNumber *)idSignal{
    FMDatabase *database = [IRDatabase openDatabase];
    [database open];
    [database executeUpdate:@"DELETE FROM signal WHERE id = ? " ,idSignal];
    [database close];
    return TRUE;
}

+(NSArray*)getListSignal{
    FMDatabase *database  = [IRDatabase openDatabase];
    [database open];
    FMResultSet *cursor = [database executeQuery:@"SELECT id,name,deviceid,format,freq,hostname,type,data,custom FROM signal"];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    while ([cursor next]) {
        int action_id = [cursor intForColumnIndex:0];
        NSString *name = [cursor stringForColumnIndex:1];
        NSString *deviceid = [cursor stringForColumnIndex:2];
        NSString *format = [cursor stringForColumnIndex:3];
        NSNumber *freq = [NSNumber numberWithLong:[cursor intForColumnIndex:4]];
        NSString *hostname = [cursor stringForColumnIndex:5];
        NSString *type = [cursor stringForColumnIndex:6];
        NSString *data = [cursor stringForColumnIndex:7];
//        NSString *custom = [cursor stringForColumnIndex:8];
        
        NSArray * rawData = [data componentsSeparatedByString:@","];
        NSMutableArray *rawIntData = [[NSMutableArray alloc]init];
        for (int i=0; i<[rawData count]; i++) {
            int value = [[rawData objectAtIndex:i] intValue];
            [rawIntData addObject:[NSNumber numberWithInt:value]];
        }
        
        NSMutableDictionary *infoSignal = [[NSMutableDictionary alloc]init];
        [infoSignal setObject:name forKey:@"name"];
        [infoSignal setObject:deviceid forKey:@"deviceid"];
        [infoSignal setObject:format forKey:@"format"];
        [infoSignal setObject:freq forKey:@"freq"];
        [infoSignal setObject:hostname forKey:@"hostname"];
        [infoSignal setObject:type forKey:@"type"];
        [infoSignal setObject:rawIntData forKey:@"data"];
        
        IRSignal *signal = [[IRSignal alloc]initWithDictionary:infoSignal];
        ItemAction *action = [[ItemAction alloc]init];
        action.action_id = [NSNumber numberWithInt:action_id];
        action.signal = signal;
        
        [result addObject:action];
    }
    [database close];
    return result;
}

+(FMDatabase *)openDatabase{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"IRWifi.sqlite"];
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    return database;
}

@end
