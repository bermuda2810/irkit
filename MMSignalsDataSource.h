#import <Foundation/Foundation.h>
#import <IRKit/IRKit.h>
#import "ItemAction.h"
@interface MMSignalsDataSource : NSObject<UITableViewDataSource>

- (void)addSignalsObject: (ItemAction*) signal;
- (ItemAction*)objectAtIndex: (NSUInteger) index;

@end
