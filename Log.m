//
//  Log.m
//  IRWifi
//
//  Created by VietBQ on 5/13/14.
//  Copyright (c) 2014 Nadia Viet Nam. All rights reserved.
//

#import "Log.h"

NSString *sp(NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    NSString *str = [[NSString alloc] initWithFormat:format arguments:args]; // ARC
    va_end(args);
    return str;
}
