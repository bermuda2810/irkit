
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// FMDB
#define COCOAPODS_POD_AVAILABLE_FMDB
#define COCOAPODS_VERSION_MAJOR_FMDB 2
#define COCOAPODS_VERSION_MINOR_FMDB 3
#define COCOAPODS_VERSION_PATCH_FMDB 0

// FMDB/common
#define COCOAPODS_POD_AVAILABLE_FMDB_common
#define COCOAPODS_VERSION_MAJOR_FMDB_common 2
#define COCOAPODS_VERSION_MINOR_FMDB_common 3
#define COCOAPODS_VERSION_PATCH_FMDB_common 0

// FMDB/standard
#define COCOAPODS_POD_AVAILABLE_FMDB_standard
#define COCOAPODS_VERSION_MAJOR_FMDB_standard 2
#define COCOAPODS_VERSION_MINOR_FMDB_standard 3
#define COCOAPODS_VERSION_PATCH_FMDB_standard 0

// IRKit
#define COCOAPODS_POD_AVAILABLE_IRKit
#define COCOAPODS_VERSION_MAJOR_IRKit 1
#define COCOAPODS_VERSION_MINOR_IRKit 0
#define COCOAPODS_VERSION_PATCH_IRKit 1

// ISHTTPOperation
#define COCOAPODS_POD_AVAILABLE_ISHTTPOperation
#define COCOAPODS_VERSION_MAJOR_ISHTTPOperation 1
#define COCOAPODS_VERSION_MINOR_ISHTTPOperation 1
#define COCOAPODS_VERSION_PATCH_ISHTTPOperation 1

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

